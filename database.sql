CREATE table pastes(
	paste_id VARCHAR(8) PRIMARY KEY,
	content TEXT	
	)

CREATE OR REPLACE FUNCTION GetContent(inputPasteID VARCHAR(8))
	RETURNS TABLE(
		content TEXT
	) AS $$
	BEGIN
		RETURN QUERY SELECT content FROM pastes WHERE paste_id = inputPasteID;
	END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION InsertContent(inputPasteID VARCHAR(8), inputContent TEXT)
	RETURNS INTEGER AS $$
	BEGIN
		INSERT INTO pastes(paste_id, content) VALUES (inputPasteID, inputContent) ;
	RETURN 0;
	END;
$$ LANGUAGE plpgsql;
	
